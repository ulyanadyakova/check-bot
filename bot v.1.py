from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters)
from telegram import (ReplyKeyboardMarkup)
import logging
import zxing
import requests
import re
import pandas as pd
from datetime import datetime
import json
import psycopg2
import os
from dotenv import load_dotenv



load_dotenv()

# telegram bot token
bot = os.getenv("TOKEN")

# nalog.ru
your_phone = os.getenv("PHONE")  # login nalog.ru
pwd = os.getenv("PASSWORD")  # password nalog.ru


# logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

logger = logging.getLogger(__name__)


# proxy
REQUEST_KWARGS = {
    'proxy_url': os.getenv("PROXY"),
    'urllib3_proxy_kwargs': {
        'username': os.getenv("USPROXY"),
        'password': os.getenv("PASSPROXY"),
    }
}


# connection to db
con = psycopg2.connect(
        database="checksdb",
        user="xredian",
        password="",
        host="localhost",
        port="5432"
    )
con.autocommit = True
cur = con.cursor()


def start(bot, update):
    reply_keyboard = [['/start', '/help', '/purchases']]
    update.message.reply_text(
        "Hi, I could save the information from your checks now. Also I'll be able to show analytics about your purchases later :)", reply_markup=ReplyKeyboardMarkup(reply_keyboard))


def help(bot, update):
    update.message.reply_text("Hey, you can upload a photo of your check to me, so I'll scan the QR-code and give you the analytics about your shopping. Enjoy :)")


def echo(bot, update):
    text = update.message.text
    if text == '/start':
        return start(bot, update)
    elif text == 'help':
        return help(bot, update)
    else:
        update.message.reply_text("I don't understand you. Please write /start or "
                                  "/help.")
        logger.info(update)


def error(bot, update, error):
    logger.warning('Update "{u}" caused error "{e}"'.format(u=update, e=error))


# uploading photo
def photo(bot, update):
    file = bot.getFile(update.message.photo[-1].file_id)
    global file_name
    file_name = file.download('/Users/xredian/project1/shopping-bot/' + str(update.message.photo[-1].file_id) + '.png')
    update.message.reply_text('Got it!\n\n' + file_name)
    return qr(bot)


# reading qr-code
def qr(bot):
    reader = zxing.BarCodeReader()
    barcode = reader.decode(file_name)
    parsed = barcode.parsed
    t = re.findall(r't=(\w+)', parsed)[0]
    s = re.findall(r's=(\w+)', parsed)[0]
    fn = re.findall(r'fn=(\w+)', parsed)[0]
    i = re.findall(r'i=(\w+)', parsed)[0]
    fp = re.findall(r'fp=(\w+)', parsed)[0]
    s1 = int(s)  # str to int to convert rubles to pennies
    # checking the existence of check
    headers = {'Device-Id': '', 'Device-OS': ''}
    payload = {'fiscalSign': fp, 'date': t, 'sum': s1 * 100}
    requests.get('https://proverkacheka.nalog.ru:9999/v1/ofds/*/inns/*/fss/' + fn + '/operations/1/tickets/' + i, params=payload, headers=headers, auth=(your_phone, pwd))
    # receiving information from check
    global request_info
    request_info = requests.get('https://proverkacheka.nalog.ru:9999/v1/inns/*/kkts/*/fss/' + fn + '/tickets/' + i + '?fiscalSign=' + fp + '&sendToEmail=no', headers=headers, auth=(your_phone, pwd))
    products = request_info.json()
    my_products = pd.DataFrame(products['document']['receipt']['items'])
    my_products['price'] = my_products['price'] / 100
    my_products['sum'] = my_products['sum'] / 100
    datetime_check = datetime.strptime(t, '%Y%m%dT%H%M%S')
    my_products['date'] = datetime_check
    my_products.set_index('date', inplace=True)
    pd.set_option('display.max_columns', 30)
    '''# post check info to the bot
    check_info = str(my_products.head())
    update.message.reply_text(check_info)'''
    return pars()


def pars():
    parsed_string = json.loads(request_info.text)
    global date
    date = parsed_string["document"]["receipt"]["dateTime"]
    date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S')
    pr = parsed_string["document"]["receipt"]["items"]
    i = 0
    while i < len(pr):
        global sum
        sum = int(pr[i]["sum"])/100
        global quantity
        quantity = pr[i]["quantity"]
        global name
        name = pr[i]["name"]
        global price
        price = int(pr[i]["price"])/100
        sql()
        i += 1


def sql():
    try:
        cur.execute("INSERT INTO checks(datetime, sum, price, name, quantity) VALUES (%s, %s, %s, %s, %s)", (date, sum, price, name, quantity))
    except:
        print("I can't insert new data!")


def select(bot, update):
    try:
        cur.execute("SELECT datetime, sum, price, name, quantity FROM checks;")
    except:
        print("I can't select new data!")
    sel = cur.fetchall()
    info = ""
    for row in sel:
        heading = ("date " + "sum " + "price " + "name " + "quantity" + "\n")
        dat = (str(row[0:][0]))
        su = (str(row[0:][1]))
        pri = (str(row[0:][2]))
        na = (str(row[0:][3]))
        qua = (str(row[0:][4]))
        info = (str(info) + dat + " " + su + " " + pri + " " + na + " " + qua + "\n")
    update.message.reply_text(heading + info)


def main():
    updater = Updater(bot, request_kwargs=REQUEST_KWARGS)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(MessageHandler(Filters.text, echo))
    dp.add_handler(MessageHandler(Filters.photo, photo))
    dp.add_handler(CommandHandler('purchases', select))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
