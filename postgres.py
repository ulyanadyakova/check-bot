import psycopg2

con = psycopg2.connect(
  database="checksdb",
  user="xredian",
  password="",
  host="localhost",
  port="5432"
)

print("Database opened successfully")

cur = con.cursor()
try:
  cur.execute("CREATE TABLE checks (datetime timestamp without time zone, name text, price numeric, quantity integer, sum numeric);")  # need to change dateTime format to timestamp
except:
  print("I can't create a table!")

con.commit()
